package cn.com.smart.form.helper;

import com.mixsmart.utils.StringUtils;

import java.util.Arrays;
import java.util.Map;

/**
 * @author 乌草坡 2019-09-01
 * @since 1.0
 */
public class ListctrlHelper {

    private Map<String, Object> dataMap;

    private String[] titles;

    public ListctrlHelper(Map<String, Object> dataMap) {
        this.dataMap = dataMap;
    }

    public String getTableId() {
        return StringUtils.handleNull(dataMap.get("bind_table"));
    }

    public String getTitle() {
        return StringUtils.handleNull(dataMap.get("title"));
    }

    public String[] getColTitleArray() {
        if(null == titles) {
            String title = StringUtils.handleNull(dataMap.get("orgtitle"));
            titles = title.split("`");
        }
        return titles;
    }

    public String[] getColTypeArray() {
        String colType = StringUtils.handleNull(dataMap.get("orgcoltype"));
        return colType.split("`");
    }

    public String[] getPluginTypeArray() {
        String pluginType = StringUtils.handleNull(dataMap.get("plugintype"));
        String[] pluginTypes = pluginType.split("`");
        return handleProp(pluginTypes, getColTitleArray());
    }

    public String[] getPluginUriArray() {
        String pluginUri = StringUtils.handleNull(dataMap.get("pluginuri"));
        String[] pluginUris = pluginUri.split("`");
        return handleProp(pluginUris, getColTitleArray());
    }

    public String[] getFieldNameArray() {
        String fieldName = StringUtils.handleNull(dataMap.get("bind_table_field"));
        String[] fieldNames = fieldName.split("`");
        return fieldNames;
    }

    public String[] getReadonlyArray() {
        String readonly = StringUtils.handleNull(dataMap.get("field_readonly"));
        String[] array = readonly.split("`");
        return handleProp(array, getColTitleArray());
    }

    public String[] getFieldListArray() {
        String fieldList = StringUtils.handleNull(dataMap.get("list_field"));
        String[] array = fieldList.split("`");
        return handleProp(array, getColTitleArray());
    }

    public String[] getFieldSearchArray() {
        String fieldSearch = StringUtils.handleNull(dataMap.get("list_field_search"));
        String[] array = fieldSearch.split("`");
        return handleProp(array, getColTitleArray());
    }

    public String[] getFieldSortArray() {
        String fieldSort = StringUtils.handleNull(dataMap.get("list_field_sort"));
        String[] array = fieldSort.split("`");
        return handleProp(array, getColTitleArray());
    }


    /**
     * 处理属性
     * @param array
     * @param titles
     * @return
     */
    public String[] handleProp(String[] array, String[] titles) {
        if(null == array || array.length < titles.length) {
            String[] tmps = array;
            array = Arrays.copyOf(tmps, titles.length);
            for (int i = tmps.length; i < titles.length; i++) {
                array[i] = "";
            }
        }
        return array;
    }
}
