package cn.com.smart.form.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import cn.com.smart.filter.bean.FilterParam;
import cn.com.smart.form.bean.entity.TFormList;
import cn.com.smart.form.enums.*;
import cn.com.smart.form.service.FormListInstanceService;
import cn.com.smart.report.bean.entity.TReport;
import cn.com.smart.report.bean.entity.TReportProperties;
import cn.com.smart.report.bean.entity.TReportSqlResource;
import cn.com.smart.res.SQLResUtil;
import cn.com.smart.web.bean.RequestPage;
import cn.com.smart.web.bean.UserInfo;
import cn.com.smart.web.constant.enums.BtnPropType;
import cn.com.smart.web.helper.HttpRequestHelper;
import cn.com.smart.web.service.OPService;
import cn.com.smart.web.tag.bean.*;
import com.mixsmart.exception.NullArgumentException;
import com.mixsmart.utils.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.smart.bean.SmartResponse;
import cn.com.smart.form.service.FormListService;
import cn.com.smart.web.helper.DataOptionHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 表单列表控制器类
 */
@Controller
@RequestMapping("/form/list")
public class FormListController extends BaseFormController {

    private static final String VIEW_DIR = "form/list";
    
    @Autowired
    private FormListService formListServ;
    @Autowired
    private FormListInstanceService formListInstServ;

    @Autowired
    private OPService opServ;

    /**
     * 报表列表
     * @param request HttpServletRequest对象
     * @param searchParam 搜索对象
     * @param page 请求分页对象
     * @return 返回列表试图
     */
    @RequestMapping("/list")
    public ModelAndView list(HttpServletRequest request, FilterParam searchParam, RequestPage page) {
        ModelAndView modelView = new ModelAndView();
        String uri = HttpRequestHelper.getCurrentUri(request);
        SmartResponse<Object> smartResp = opServ.getDatas("get_form_list_mgr_list", searchParam, page.getStartNum(), page.getPageSize());

        CustomBtn customBtn = new CustomBtn("edit_designer", "表单list设计器", "修改表单List", "form/list/designer");
        customBtn.setSelectedType(BtnPropType.SelectType.ONE.getValue());
        customBtn.setBtnIcon("glyphicon-pencil");
        customBtn.setOpenStyle(BtnPropType.OpenStyle.OPEN_SELF);
        customBtns = new ArrayList<CustomBtn>(1);
        customBtns.add(customBtn);
        delBtn = new DelBtn("form/list/delete", "确定要删除选中的表单列表吗？删除后数据将无法恢复.",uri,null, null);
        refreshBtn = new RefreshBtn(uri, null,null);
        pageParam = new PageParam(uri, null, page.getPage(), page.getPageSize());

        ModelMap modelMap = modelView.getModelMap();
        modelMap.put("smartResp", smartResp);
        modelMap.put("customBtns", customBtns);
        modelMap.put("searchParam", searchParam);
        modelMap.put("delBtn", delBtn);
        modelMap.put("refreshBtn", refreshBtn);
        modelMap.put("pageParam", pageParam);
        modelView.setViewName(VIEW_DIR+"/list");
        return modelView;
    }

    /**
     * 保存表单列表设计属性
     * @param session
     * @param formList
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces="application/json; chartset=UTF-8")
    @ResponseBody
    public SmartResponse<String> save(HttpSession session, TFormList formList) {
        UserInfo userInfo = super.getUserInfoFromSession(session);
        formList.setUserId(userInfo.getId());
        SmartResponse<String> smartResp = formListServ.saveOrUpdate(formList);
        return smartResp;
    }

    /**
     * 删除表单列表
     * @param id 表单列表ID
     * @return 返回删除结果
     */
    @RequestMapping(value = "/delete", produces = "application/json; chartset=UTF-8")
    @ResponseBody
    public SmartResponse<String> delete(String id) {
        return formListServ.delete(id);
    }


    /**
     * 表单列表设计
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("/designer")
    public ModelAndView designer(HttpServletRequest request, String id) {
        ModelAndView modelView = new ModelAndView();
        if(StringUtils.isNotEmpty(id)) {
            TFormList formList = formListServ.queryAssoc(id);
            modelView.getModelMap().put("objBean", formList);
        }
        modelView.setViewName(VIEW_DIR+"/designer");
        return modelView;
    }
    
    /**
     * 获取数据范围
     * @return
     */
    @RequestMapping(value = "/dataScope", method = RequestMethod.GET)
    @ResponseBody
    public SmartResponse<Object> dataScope() {
        return DataOptionHelper.labelValueToResponse(FormListDataScopeType.getOptions());
    }

    /**
     * 获取搜索表达式类型
     * @return
     */
    @RequestMapping(value = "/searchExpType", method = RequestMethod.GET)
    @ResponseBody
    public SmartResponse<Object> searchExpType() {
        return DataOptionHelper.labelValueToResponse(SearchExpType.getOptions());
    }

    /**
     * 获取表单列表类型
     * @return
     */
    @RequestMapping(value = "/formListType", method = RequestMethod.GET)
    @ResponseBody
    public SmartResponse<Object> formListType() {
        return DataOptionHelper.labelValueToResponse(FormListType.getOptions());
    }

    /**
     * 获取搜索插件类型
     * @return
     */
    @RequestMapping(value = "/searchPluginType", method = RequestMethod.GET)
    @ResponseBody
    public SmartResponse<Object> searchPluginType() {
        return DataOptionHelper.labelValueToResponse(SearchPluginType.getOptions());
    }

    /**
     * 报表列表
     * @param request HttpServlet请求对象
     * @param id 表单列表ID
     * @param page 分页对象
     * @return 返回列表视图
     */
    @RequestMapping("/instance")
    public ModelAndView list(HttpServletRequest request, String id, RequestPage page) {
        ModelAndView modelView = new ModelAndView();
        if(com.mixsmart.utils.StringUtils.isEmpty(id)) {
            throw new NullArgumentException("表单List Id参数为空");
        }
        ModelMap modelMap = modelView.getModelMap();
        formListInstServ.handleListView(id, modelMap, page, request);
        modelView.setViewName(VIEW_DIR+"/instance");
        return modelView;
    }

    /**
     * 导出数据
     * @param request HttpServletRequest请求对象
     * @param id 表单列表ID
     * @return
     */
    @RequestMapping("/export")
    public ResponseEntity<byte[]> export(HttpServletRequest request, String id) {
        return formListInstServ.export(id, request);
    }
}
