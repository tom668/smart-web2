package cn.com.smart.form.parser;

import cn.com.smart.form.enums.FormControlFieldType;
import cn.com.smart.form.enums.FormDataSourceType;
import cn.com.smart.form.list.AbstractFormListFieldParser;
import cn.com.smart.form.list.bean.AbstractListFieldProp;
import cn.com.smart.form.list.bean.OptionListFieldProp;
import cn.com.smart.form.list.helper.FormListParseHelper;
import cn.com.smart.web.bean.LabelValue;
import com.mixsmart.enums.YesNoType;
import com.mixsmart.utils.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 解析下拉框
 * @author lmq
 * @version 1.0 
 * @since 1.0
 * 2015年7月4日
 */
@Component
public class SelectParser extends AbstractFormListFieldParser implements IFormParser {

	@Override
	public String getPlugin() {
		return "select";
	}

	@Override
	public String parse(Map<String, Object> dataMap) {
		if(null == dataMap || dataMap.size()<1) {
			return null;
		}
		String content = StringUtils.handleNull(dataMap.get("content"));
		String fromData = StringUtils.handleNull(dataMap.get("from_data"));
		String dynamicLoad = StringUtils.handleNull(dataMap.get("dynamicload"));
		String formControlField = StringUtils.handleNull(dataMap.get("form_control_field"));
		String readonly = StringUtils.handleNull(dataMap.get("is_readyonly"));
		FormControlFieldType fcfType = FormControlFieldType.getObjByValue(formControlField);
		String fcfClassName = "";
		if(null != fcfType) {
			fcfClassName = fcfType.getPluginValue();
		}
		if(YesNoType.YES.getStrValue().equals(readonly)) {
			fcfClassName += " select-readonly";
		}
		FormDataSourceType sourceType = FormDataSourceType.getObj(fromData);
		switch (sourceType) {
		case DICT:
		case CUSTOM_URI:
			if(YesNoType.YES.getStrValue().equals(dynamicLoad)) {
				StringBuilder strBuild = new StringBuilder();
				strBuild.append("<select name=\""+StringUtils.handleNull(dataMap.get("bind_table_field"))+"\" id=\""+dataMap.get("bind_table_field")+"\" data-label-name=\""+dataMap.get("title")+"\" ");
				strBuild.append(" class=\""+dataMap.get("class")+" cnoj-select "+StringUtils.handleNull(fcfClassName)+" \"");
				strBuild.append(" style=\""+dataMap.get("style")+"\" data-uri=\""+dataMap.get("data_uri")+"\"></select>");
				content = strBuild.toString();
				break;
			}
		default:
			if(!StringUtils.isEmpty(content)) {
				content = content.replaceAll("<select([^>].*?)>", "<select class=\""+dataMap.get("class")+" "+StringUtils.handleNull(fcfClassName)+"\" name=\"111\" >");
				content = content.replaceAll("(leipiplugins=\".*?\")|(field.*?=\".*?\")|(org.*?=\".*?\")|(from.*?=\".*?\")|(bind_.*?=\".*?\")", "");
				content = content.replaceAll("name=\".*?\"", "name=\""+dataMap.get("bind_table_field")+"\" id=\""+dataMap.get("bind_table_field")+"\" data-label-name=\"" + dataMap.get("title") + "\" style=\""+dataMap.get("style")+"\"");
			}
			break;
		}
		return content;
	}

	@Override
	protected AbstractListFieldProp parseListField(Map<String, Object> dataMap) {
		String content = StringUtils.handleNull(dataMap.get("content"));
		String fromData = StringUtils.handleNull(dataMap.get("from_data"));
		String dynamicLoad = StringUtils.handleNull(dataMap.get("dynamicload"));
		OptionListFieldProp listFieldProp = new OptionListFieldProp();
		FormDataSourceType sourceType = FormDataSourceType.getObj(fromData);
		switch (sourceType) {
			case DICT:
			case CUSTOM_URI:
				if(YesNoType.YES.getStrValue().equals(dynamicLoad)) {
					listFieldProp.setUrl(StringUtils.handleNull(dataMap.get("data_uri")));
					break;
				}
			default:
				if(!StringUtils.isEmpty(content)) {
					Pattern pattern = Pattern.compile("<option value=\"(.*?)\">(.*?)</option>");
					Matcher matcher = pattern.matcher(content);
					List<LabelValue> options = new ArrayList<>();
					while(matcher.find()){
						options.add(new LabelValue(matcher.group(2), matcher.group(2)));
					}
				}
				break;
		}
		if(FormListParseHelper.parseCommonField(dataMap, listFieldProp)) {
			return listFieldProp;
		}
		return null;
	}
}
