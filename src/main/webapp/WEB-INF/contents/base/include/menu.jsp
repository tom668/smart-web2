<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="cnoj" uri="/cnoj-tags" %>
<jsp:include page="./header.jsp"></jsp:include>
	<script src="${pageContext.request.contextPath}/js/easyui.tabs.contextmenu.js" type="text/javascript" charset="UTF-8"></script>
   <link href="${pageContext.request.contextPath}/css/menu.css" rel="stylesheet" />
   <script type="text/javascript" src="${pageContext.request.contextPath}/js/left-menu.js"></script>

<div class="cnoj-menu" id="menu-nav">
	<h5 class="menu-title"><i class="glyphicon glyphicon-th-large"></i> 功能菜单</h5>
	<div id="menu-sub-nav">
		<cnoj:menu smartResp="${menuRes }" />
	</div>
</div>
<div class="menu-resizer">
    <div class="menu-resizer-toggler"></div>
</div>
<script type="text/javascript">
  var forward = '${forward}';
  $(function(){
	  if(utils.isNotEmpty(forward)) {
		  var is = false;
		  $("#menu-sub-nav a").each(function() {
			  var $this = $(this);
			  var uri = $this.data("uri");
			  if(utils.isNotEmpty(uri) && ( forward == uri || forward.indexOf(uri)>-1)) { 
				  $this.parentsUntil("#menu-sub-nav","ul").trigger("click");
				  $this.data("uri",forward);
				  $this.trigger("click");
				  $this.data("uri",uri);
				  is = true;
				  return 0;
			  }
		  });
		 /* if(!is) {
			  BootstrapDialogUtil.warningAlert("对不起，您没有权限访问指定的页面！");
		  }*/
	  }
  });
</script>