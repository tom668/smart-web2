<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/form/js/form.mapping.config.js"></script>
 <div class="wrap-content-dialog" id="config-form-mapping">
    <form class="form-horizontal" role="form" id="form-mapping" action="form/mapping/save.json" target="#main-content">
		<input type="hidden" name="id" value="${objBean.id }" />
		<div class="panel panel-default">
           <div class="panel-heading p-t-8 p-b-8 p-l-4 text-bold">表单映射配置</div>
			<table class="table table-bordered table-condensed table-sm">
			   <tbody>
			      <tr>
			         <th class="text-right" style="width: 80px;">原表单：</th>
			         <td>
			            <div class="col-sm-12 p-l-0 p-r-0">
							<select class="form-control cnoj-select require" name="formId" data-uri="op/query/select_all_form_list.json" id="source-form" data-default-value="${objBean.formId}">
								<option value="">请选择</option>
							</select>
						</div>
			         </td>
					  <th class="text-right" style="width: 80px;">目标表单：</th>
					  <td>
						  <div class="col-sm-12 p-l-0 p-r-0">
							  <select class="form-control cnoj-select require" name="targetFormId" data-uri="op/query/select_all_form_list.json" id="target-form" data-default-value="${objBean.targetFormId}">
								  <option value="">请选择</option>
							  </select>
						  </div>
					  </td>
			      </tr>
				  <tr class="bg-color-pd">
					  <td colspan="4">
						  <div class="col-sm-6 p-t-5 p-b-3 p-l-0 p-r-0 color-pd text-bold">映射字段</div>
						  <div class="col-sm-6 p-t-5 p-b-3 p-r-5 text-right"><button type="button" class="add-mapping-field btn btn-primary btn-xs"><i class="glyphicon glyphicon-plus-sign"></i> 添加</button></div>
					  </td>
				  </tr>
				  <tr>
					  <td colspan="4" class="seamless-embed-table">
						  <div style="overflow: auto;height: 350px;">
							  <table class="table table-bordered table-condensed table-sm" id="form-mapping-field-table">
								  <thead>
								  <tr class="ui-state-default" style="border: none;">
									  <th style="width: 40px;">序号</th>
									  <th>原字段</th>
									  <th>目标字段</th>
									  <th style="width: 40px">操作</th>
								  </tr>
								  </thead>
								  <tbody>
									<c:if test="${not empty objBean && not empty objBean.fieldMappings}">
										<c:forEach items="${objBean.fieldMappings }" var="fieldMap" varStatus="st">
										<tr>
										  <td class="seq-num text-right">
											  <input type="hidden" name="fieldMappings[${st.index}].id" value="${fieldMap.id }" />
											  <input type="hidden" class="sort-order" name="fieldMappings[${st.index}].sortOrder" value="${fieldMap.sortOrder }" />
											  <span class="sort-order-label">${st.index+1 }</span>
										  </td>
										  <td>
											  <select class="form-control source-form-field require" name="fieldMappings[${st.index}].fieldId" data-default-value="${fieldMap.fieldId}">
												  <option value="">请选择</option>
											  </select>
										  </td>
										  <td>
											  <select class="form-control target-form-field require" name="fieldMappings[${st.index}].targetFieldId" data-default-value="${fieldMap.targetFieldId}">
												  <option value="">请选择</option>
											  </select>
										  </td>
										  <td class="del-td" id="del-field-mapping${st.index}" class="text-center">
											  <button type="button" title="删除" class="delete-field-mapping text-center" style="float: none;font-size: 18px;" data-dismiss="tr1" aria-label="Close"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
										  </td>
									  </tr>
										</c:forEach>
									</c:if>
								  </tbody>
							  </table>
						  </div>
					  </td>
				  </tr>
			   </tbody>
	        </table>
          </div><!-- panel -->
          <div class="text-center p-t-10">
		    <button type="button" class="btn btn-primary cnoj-data-submit" data-refresh-uri="form/mapping/list">
		    <i class="glyphicon glyphicon-ok-sign"></i> 保存</button>
	      </div>
	  </form>
 </div>
 <script type="text/javascript">
    //plugins/form/js/form.mapping.config.js
    setTimeout("loadJs()", 200);
    function loadJs(){
    	$("#config-form-mapping").formMappingConfigListener();
    }
 </script>