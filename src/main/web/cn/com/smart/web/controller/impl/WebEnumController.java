package cn.com.smart.web.controller.impl;

import cn.com.smart.bean.SmartResponse;
import cn.com.smart.web.constant.enums.BtnPropType;
import cn.com.smart.web.controller.base.BaseController;
import cn.com.smart.web.helper.DataOptionHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 获取web模块中的枚举项
 * @author 乌草坡 2019-09-01
 * @since 1.0
 */
@RestController
@RequestMapping("/web/enum")
public class WebEnumController extends BaseController {

    /**
     * 获取点击按钮时选中列表的类型
     * @return
     */
    @RequestMapping(value = "/btn/selectedListType", method = RequestMethod.GET)
    public SmartResponse<Object> getBtnSelectedListType() {
        return DataOptionHelper.labelValueToResponse(BtnPropType.SelectType.getOptions());
    }

    /**
     * 获取点击按钮时的支持触发的类型
     * @return
     */
    @RequestMapping(value = "/btn/openType", method = RequestMethod.GET)
    public SmartResponse<Object> getBtnOpenType() {
        return DataOptionHelper.labelValueToResponse(BtnPropType.OpenStyle.getOptions());
    }

}
